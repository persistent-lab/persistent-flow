document.addEventListener("DOMContentLoaded", function() {
    const taskInput = document.getElementById('taskInput');
    const taskOwnerInput = document.getElementById('taskOwnerInput');
    const stageSelect = document.getElementById('stageSelect');
    const stepSelect = document.getElementById('stepSelect');
    const statusSelect = document.getElementById('statusSelect');
    const taskTableBody = document.getElementById('taskTableBody');

    window.addTask = function() {
        const taskName = taskInput.value.trim();
        const taskOwner = taskOwnerInput.value.trim();
        if (taskName && taskOwner && 
            stageSelect.value !== "Choose Stage" && 
            stepSelect.value !== "Choose Step" && 
            statusSelect.value !== "Choose Status") {
            const rowCount = taskTableBody.rows.length;
            const row = taskTableBody.insertRow(rowCount);
            const cell1 = row.insertCell(0);
            const cell2 = row.insertCell(1);
            const cell3 = row.insertCell(2);
            const cell4 = row.insertCell(3);
            const cell5 = row.insertCell(4);
            const cell6 = row.insertCell(5);
            const cell7 = row.insertCell(6);
            const cell8 = row.insertCell(7);

            cell1.innerHTML = rowCount + 1;
            cell2.innerHTML = taskName;
            cell3.innerHTML = stageSelect.value;
            cell4.innerHTML = stepSelect.value;
            cell5.innerHTML = statusSelect.value;
            cell6.innerHTML = taskOwner;
            cell7.innerHTML = '00:00:00';
            cell7.classList.add('time-execution');
            cell8.innerHTML = `<button class="btn btn-red">Start</button>`;

            setupButtonActions(row);
        } else {
            // Optional: Add an alert or visual feedback when not all fields are filled
            alert('Please fill all fields to add a task.');
        }
    };

    function setupButtonActions(row) {
        const startButton = row.querySelector('.btn-red');
        let seconds = 0;
        let interval = null;
        const timeExecutionCell = row.querySelector('.time-execution');

        startButton.addEventListener('click', () => {
            if (startButton.classList.contains('btn-red')) {
                startButton.classList.remove('btn-red');
                startButton.classList.add('btn-orange');
                startButton.innerText = 'Wait';

                interval = setInterval(() => {
                    seconds++;
                    timeExecutionCell.innerText = formatTime(seconds);
                }, 1000);
            } else if (startButton.classList.contains('btn-orange')) {
                clearInterval(interval);
                startButton.classList.remove('btn-orange');
                startButton.classList.add('btn-green');
                startButton.innerText = 'Done';
                row.classList.add('task-done'); // Aplica a classe para tachar a linha
            }
        });
    }

    function formatTime(totalSeconds) {
        const hours = Math.floor(totalSeconds / 3600);
        const minutes = Math.floor((totalSeconds % 3600) / 60);
        const seconds = totalSeconds % 60;
        return [hours, minutes, seconds].map(v => v < 10 ? '0' + v : v).join(':');
    }
});
